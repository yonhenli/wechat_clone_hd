import 'package:flutter/material.dart';

import '../constants.dart' show AppColors, AppStyles, Constants;
import './conversation_detail_args.dart';

class ConversationDetailPage extends StatefulWidget {
  @override
  _ConversationDetailPageState createState() => _ConversationDetailPageState();
}

class _ConversationDetailPageState extends State<ConversationDetailPage> {
  final maxChatBoxLinesForDisplay = 4;
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final paddingH = 16.0;
    final paddingV = 12.0;
    final chatBoxRadius = 4.0;
    final ConversationDetailArgs args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text(args.title, style: AppStyles.TitleStyle),
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
                IconData(
                  0xe64c,
                  fontFamily: Constants.IconFontFamily,
                ),
                size: Constants.ActionIconSize + 4.0,
                color: const Color(AppColors.ActionIconColor))),
        elevation: 0.0,
        brightness: Brightness.light,
        backgroundColor: const Color(AppColors.PrimaryColor),
        actions: [
          IconButton(
              onPressed: () {
                print('打开显示更多的下拉菜单');
              },
              icon: Icon(
                  IconData(
                    0xe609,
                    fontFamily: Constants.IconFontFamily,
                  ),
                  size: Constants.ActionIconSize + 4.0,
                  color: const Color(AppColors.ActionIconColor))),
          SizedBox(width: 16.0)
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              color: const Color(AppColors.BackgroundColor),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: paddingH, vertical: paddingV),
            decoration: BoxDecoration(
                color: const Color(AppColors.ChatBoxBg),
                border: Border(
                    top: BorderSide(
                        color: const Color(AppColors.DividerColor),
                        width: Constants.DividerWidth))),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                IconButton(
                    onPressed: () {
                      print('语音文字切换');
                    },
                    icon: Icon(
                        IconData(
                          0xe7e2,
                          fontFamily: Constants.IconFontFamily,
                        ),
                        size: Constants.ActionIconSizeLarge,
                        color: const Color(AppColors.ActionIconColor))),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(left: 4.0, right: 4.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(chatBoxRadius)
                    ),
                    child: LayoutBuilder(
                      builder: (context, size) {
                        // 计算当前的文本需要占用的行数
                        TextSpan _text = TextSpan(
                          text: _controller.text,
                          style: AppStyles.ChatBoxTextStyle
                        );

                        TextPainter _tp = TextPainter(
                          text: _text,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.left
                        );
                        _tp.layout(maxWidth: size.maxWidth);

                        final _lines = (_tp.size.height / _tp.preferredLineHeight).ceil();

                        return TextField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: const EdgeInsets.all(10.0)
                          ),
                          controller: _controller,
                          cursorColor: const Color(AppColors.ChatBoxCursorColor),
                          maxLines: _lines < maxChatBoxLinesForDisplay ? null : maxChatBoxLinesForDisplay,
                          style: AppStyles.ChatBoxTextStyle,
                        );
                      },
                    ),
                  ),
                ),
                IconButton(
                    onPressed: () {
                      print('表情包');
                    },
                    icon: Icon(
                        IconData(
                          0xe60c,
                          fontFamily: Constants.IconFontFamily,
                        ),
                        size: Constants.ActionIconSizeLarge,
                        color: const Color(AppColors.ActionIconColor))),
                IconButton(
                    onPressed: () {
                      print('显示更多功能');
                    },
                    icon: Icon(
                        IconData(
                          0xe616,
                          fontFamily: Constants.IconFontFamily,
                        ),
                        size: Constants.ActionIconSizeLarge,
                        color: const Color(AppColors.ActionIconColor)))
              ],
            ),
          )
        ],
      ),
    );
  }
}
