import 'package:flutter/material.dart';

import './constants.dart' show AppColors, Routes;
import './home/home_screen.dart';
import './conversation/conversation_detail_page.dart';

void main() => runApp(MaterialApp(
      title: '微信',
      theme: ThemeData.light().copyWith(
        primaryColor: Color(AppColors.PrimaryColor),
        cardColor: const Color(AppColors.CardBgColor),
        backgroundColor: Color(AppColors.BackgroundColor),
      ),
      initialRoute: Routes.Home,
      routes: {
        Routes.Home: (context) => HomeScreen(),
        Routes.Conversation: (context) => ConversationDetailPage()
      },
    ));
